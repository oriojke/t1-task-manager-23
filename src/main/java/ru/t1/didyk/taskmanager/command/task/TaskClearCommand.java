package ru.t1.didyk.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public void execute() {
        @Nullable final String userId = getUserId();
        System.out.println("[CLEAR TASKS]");
        getTaskService().clear(userId);
    }

}
