package ru.t1.didyk.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Update project by id.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-update-by-id";
    }

    @Override
    public void execute() {
        @Nullable final String userId = getUserId();
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        getProjectService().updateById(userId, id, name, description);
    }

}
